from django.conf.urls import url
from django.contrib import admin

from .views import (
	downloader_home,
	)

urlpatterns = [
	url(r'^$', downloader_home),
]