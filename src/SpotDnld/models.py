from __future__ import unicode_literals

from django.db import models


class Permission(models.Model):
    user = models.CharField(max_length=120)
    downloads = models.BooleanField()
    player = models.BooleanField()
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)


    # Set object name
    def __unicode__(self):
        return self.user

    def __str__(self):
        return self.user

# ========================================================================== #
#                       Download params configuration                        #
# ========================================================================== #
# ========================================================================== #
#               DESCRIPTION                             |    MODEL TYPE      #
# ========================================================================== #
# Slide check for list or bunch of songs models         |    BooleanField    #
# TextBox to paste URLS (maybe just a button to paste)  |    TextField       #
# Slide bar to set quality                              |    IntegerField    #
# Selector for Audio format                             |    Select          #
# Slide check for set or not album art tag              |    BooleanField    #
# ========================================================================== #
class Dnld_conf(models.Model):
    mode = models.BooleanField()
    content = models.TextField()
    quality = models.IntegerField()
    format = models.IntegerField()
    albumart = models.BooleanField()

    # Set object name
    def __unicode__(self):
        return 'Downloads configuration'

    def __str__(self):
        return 'Downloads configuration'