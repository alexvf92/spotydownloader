from django.contrib import admin

# Register download configuration model
from .models import Permission

class PermissionsModelAdmin(admin.ModelAdmin):
	list_display = ['user', 'downloads', 'player', 'updated', 'timestamp']
	list_filter = ['downloads', 'player', 'updated']
	class Meta:
		model = Permission

admin.site.register(Permission, PermissionsModelAdmin)