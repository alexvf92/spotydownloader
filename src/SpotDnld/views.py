from django.shortcuts import render, get_object_or_404

# Create your views here.
def downloader_home(request):
	# if request.user.is_authenticated():
	# 	context = {
	# 		'title' : 'Authenticated!!'
	# 	}
	# else:
	# 	context = {
	# 		'title' : 'Not authenticated!!'
	# 	}
	context = {
			'title' : 'Music downloader'
		}
	return render(request, 'index.html', context)